# Deploying PWAs to Netlify


## How to Deploy an Angular App to Netlify


### Build the app

Build the Angular app with PWA enabled. Here's a quick instruction:

* [Angular and PWA](https://gitlab.com/pwas/angular)



### Deploy the app

You can deploy the build either manually or via Continuous deployment setup to Netlify.

For CD, the best thing to do is to create `netlify.toml` file and put it at the root of the repo.
Here's the simplest example:

    # In this example, "myapp" is the app folder, which includes package.json.
    [build]
      base    = "myapp"
      publish = "myapp/dist"
      command = "npm i && npm run build"


### Configure

For a SPA app which use routes/paths, you will need to set redirect rules
to be able to handle URLs that are not directed to the root "/".
Create `_redirects` file and copy it over to the root of the build dir (where the index.html file is) 
during the build process (e.g., via .angular-cli.json). 
Here's a simple example:

    /*    /index.html   200


You may also have to set the CORS header using `_headers` file, if necessary.

    /*
    Access-Control-Allow-Origin: *



### Netlify setup

1. Set up a custom domain.
1. Enable HTTPS.
1. And, optinally, enable HTTP-to-HTTPS redirect setting.

I also generally enable "Pre-rendering" for my Angular apps.






